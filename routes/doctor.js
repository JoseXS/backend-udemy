var express = require('express');
var mdAutentication = require('../middlewares/autenticacion');
var app = express();
var Doctor = require('../models/doctor');

// ===================================
// Get All: Obtener todos los doctores
// ===================================

app.get('/', (req, res) => {
    var desde = req.query.desde || 0;
    desde = Number(desde);
    Doctor.find({})
        .skip(desde)
        .limit(5)
        .populate('user', 'name email img')
        .populate('hospital')
        .exec(
            (err, doctors) => {
                if (err) {
                    return res.status(500).json({
                        ok: false,
                        mensaje: 'Error cargando doctores',
                        errors: err
                    });
                }
                Doctor.count({}, (err, conteo) => {
                    res.status(200).json({
                        ok: true,
                        doctors: doctors,
                        total: conteo
                    });
                })
            });
});

// ===================================
// Actualizar doctor
// ===================================
app.put('/:id', mdAutentication.verificaToken, (req, res) => {
    var id = req.params.id;
    var body = req.body;
    Doctor.findById(id, (err, doctor) => {
        if (err) {
            return res.status(500).json({
                ok: false,
                mensaje: 'Error al buscar doctor',
                errors: err
            });
        }
        if (!doctor) {
            return res.status(400).json({
                ok: false,
                mensaje: 'El doctor con el id ' + id + ' no existe',
                errors: { message: 'No existe un doctor con ese ID' }
            });
        }
        doctor.name = body.name;
        doctor.hospital = body.hospital;
        doctor.user = req.user._id;
        doctor.save((err, doctorSaved) => {
            if (err) {
                return res.status(400).json({
                    ok: false,
                    mensaje: 'Error al actualizar doctor',
                    errors: err
                });
            }

            res.status(200).json({
                ok: true,
                doctor: doctorSaved
            });
        });
    });

});

// ===================================
// Crear nuevo doctor
// ===================================
app.post('/', mdAutentication.verificaToken, (req, res) => {
    var body = req.body;
    var doctor = new Doctor({
        name: body.name,
        hospital: body.hospital,
        user: req.user._id
    });

    doctor.save((err, newDoctor) => {
        if (err) {
            return res.status(400).json({
                ok: false,
                mensaje: 'Error al crear doctor',
                errors: err
            });
        }
        res.status(201).json({
            ok: true,
            doctor: newDoctor
        });
    });
});

// ===================================
// Borrar un doctor por el id
// ===================================
app.delete('/:id', mdAutentication.verificaToken, (req, res) => {
    var id = req.params.id;
    Doctor.findByIdAndRemove(id, (err, doctorRemoved) => {
        if (err) {
            return res.status(500).json({
                ok: false,
                mensaje: 'Error al eliminar doctor',
                errors: err
            });
        }

        if (!doctorRemoved) {
            return res.status(400).json({
                ok: false,
                mensaje: 'No existe un doctor con ese id',
                errors: { message: 'No existe un doctor con ese id' }
            });
        }

        res.status(200).json({
            ok: true,
            doctor: doctorRemoved
        });
    });
});

module.exports = app;