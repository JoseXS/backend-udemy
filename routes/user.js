var express = require('express');
var bcrypt = require('bcryptjs');
var mdAutentication = require('../middlewares/autenticacion');
var app = express();
var User = require('../models/user');

// ===================================
// Get All: Obtener todos los users
// ===================================

app.get('/', (req, res) => {
    var desde = req.query.desde || 0;
    desde = Number(desde);
    User.find({}, 'name email img role')
        .skip(desde)
        .limit(5)
        .exec(
            (err, users) => {
                if (err) {
                    return res.status(500).json({
                        ok: false,
                        mensaje: 'Error cargando user',
                        errors: err
                    });
                }
                User.count({}, (err, conteo) => {
                    res.status(200).json({
                        ok: true,
                        users: users,
                        total: conteo
                    });
                })

            });
});

// ===================================
// Actualizar user
// ===================================
app.put('/:id', mdAutentication.verificaToken, (req, res) => {
    var id = req.params.id;
    var body = req.body;
    User.findById(id, (err, user) => {
        if (err) {
            return res.status(500).json({
                ok: false,
                mensaje: 'Error al buscar user',
                errors: err
            });
        }
        if (!user) {
            return res.status(400).json({
                ok: false,
                mensaje: 'El user con el id ' + id + ' no existe',
                errors: { message: 'No existe un user con ese ID' }
            });
        }
        user.name = body.name;
        user.email = body.email;
        user.role = body.role;
        user.save((err, userSaved) => {
            if (err) {
                return res.status(400).json({
                    ok: false,
                    mensaje: 'Error al actualizar user',
                    errors: err
                });
            }

            userSaved.password = ':D';

            res.status(200).json({
                ok: true,
                user: userSaved
            });
        });
    });

});

// ===================================
// Crear nuevo user
// ===================================
app.post('/', mdAutentication.verificaToken, (req, res) => {
    var body = req.body;
    var user = new User({
        name: body.name,
        email: body.email,
        password: bcrypt.hashSync(body.password, 10),
        img: body.img,
        role: body.role
    });

    user.save((err, newUser) => {
        if (err) {
            return res.status(400).json({
                ok: false,
                mensaje: 'Error al crear user',
                errors: err
            });
        }
        console.log(newUser);
        res.status(201).json({
            ok: true,
            user: newUser
        });
    });
});

// ===================================
// Borrar un user por el id
// ===================================
app.delete('/:id', mdAutentication.verificaToken, (req, res) => {
    var id = req.params.id;
    User.findByIdAndRemove(id, (err, userRemoved) => {
        if (err) {
            return res.status(500).json({
                ok: false,
                mensaje: 'Error al eliminar user',
                errors: err
            });
        }

        if (!userRemoved) {
            return res.status(400).json({
                ok: false,
                mensaje: 'No existe un user con ese id',
                errors: { message: 'No existe un user con ese id' }
            });
        }

        res.status(200).json({
            ok: true,
            user: userRemoved
        });
    });
});

module.exports = app;