var express = require('express');
var app = express();
var Hospital = require('../models/hospital');
var Doctor = require('../models/doctor');
var User = require('../models/user');

// ==========================
//  Busqueda total
// ==========================
app.get('/all/:search', (req, res) => {
    var search = req.params.search;
    var regex = new RegExp(search, 'i');
    Promise.all([
        searchHospital(search, regex),
        searchDoctor(search, regex),
        searchUser(search, regex)
    ]).then((responses) => {
        res.status(200).json({
            ok: true,
            hospitals: responses[0],
            doctors: responses[1],
            users: responses[2]
        });
    });
});

function searchHospital(search, regex) {
    return new Promise((resolve, reject) => {
        Hospital.find({ name: regex })
            .populate('user', 'name email img')
            .exec((err, hospitals) => {
                if (err) { reject('Error al cargar Hospitales', err); }
                resolve(hospitals);
            });
    })
}

function searchDoctor(search, regex) {
    return new Promise((resolve, reject) => {
        Doctor.find({ name: regex })
            .populate('user', 'name email img')
            .populate('hospital')
            .exec((err, doctors) => {
                if (err) { reject('Error al cargar Medicos', err); }
                resolve(doctors);
            });
    })
}

function searchUser(search, regex) {
    return new Promise((resolve, reject) => {
        User.find({}, 'name email role img')
            .or([{ 'nombre': regex }, { 'email': regex }])
            .exec((err, users) => {
                if (err) { reject('Error al cargar Usuarios', err); }
                resolve(users);
            });
    })
}

// ==========================
//  Busqueda por coleccion
// ==========================

app.get('/type/:type/:search', (req, res) => {
    var type = req.params.type;
    var search = req.params.search;
    var regex = new RegExp(search, 'i');
    var promesa;

    switch (type) {
        case 'hospitals':
            promesa = searchHospital(search, regex)
            break;
        case 'doctors':
            promesa = searchDoctor(search, regex);
            break;
        case 'users':
            promesa = searchUser(search, regex);
            break;
        default:
            return res.status(400).json({
                ok: false,
                message: 'Los tipos de busqueda solo son: usuarios, medicos y hospitales',
                error: { message: 'Tipo de coleccion no valido' }
            });
    }
    promesa.then(data => {
        return res.status(200).json({
            ok: true,
            [type]: data
        });
    });
});

module.exports = app;