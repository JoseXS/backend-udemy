var express = require('express');
var fileUpload = require('express-fileupload');
var fs = require('fs');
var app = express();

var Hospital = require('../models/hospital');
var Doctor = require('../models/doctor');
var User = require('../models/user');

app.use(fileUpload());

app.put('/:type/:id', (req, res) => {
    var type = req.params.type;
    var id = req.params.id;
    // tipos de coleccion
    var typeOk = ['hospitals', 'doctors', 'users'];
    if (typeOk.indexOf(type) < 0) {
        return res.status(400).json({
            ok: false,
            mensaje: 'El tipo de coleccion no es valida',
            errors: { message: 'El tipo de coleccion no es valida' }
        });
    }
    if (!req.files) {
        return res.status(500).json({
            ok: false,
            mensaje: 'No selecciono nada',
            errors: { message: 'Debe seleccionar una imagen' }
        });
    }

    // obtener nombre del archivo
    var file = req.files.image;
    var cutName = file.name.split('.');
    var extFile = cutName[cutName.length - 1];

    // validacion de archivos
    var extOk = ['png', 'jpg', 'gif', 'jpeg'];
    if (extOk.indexOf(extFile) < 0) {
        return res.status(500).json({
            ok: false,
            mensaje: 'Extension no valida',
            errors: { message: 'Las extensiones validas son ' + extOk.join(', ') }
        });
    }

    // nombre de archivo personalizado
    var fileName = `${id}-${new Date().getMilliseconds()}.${extFile}`;
    // mover archivo
    var path = `./uploads/${type}/${fileName}`;
    file.mv(path, (err) => {
        if (err) {
            return res.status(500).json({
                ok: false,
                mensaje: 'Error al mover archivo',
                errors: err
            });
        }

        uploadByType(type, id, fileName, res);
    });
});

function uploadByType(type, id, fileName, res) {
    switch (type) {
        case 'users':
            User.findById(id, (err, user) => {
                if (!user) {
                    return res.status(400).json({
                        ok: true,
                        mensaje: 'Usuario no existe',
                        errors: { message: 'Usuario no existe' }
                    });
                }
                var pathOld = './uploads/users/' + user.img;
                if (fs.existsSync(pathOld)) {
                    fs.unlink(pathOld);
                }
                user.img = fileName;
                user.save((err, userUpdated) => {
                    userUpdated.password = ':D';
                    return res.status(200).json({
                        ok: true,
                        mensaje: 'Imagen de usuario actualizada',
                        user: userUpdated
                    });
                });

            });
            break;
        case 'doctors':
            Doctor.findById(id, (err, doctor) => {
                if (!doctor) {
                    return res.status(400).json({
                        ok: true,
                        mensaje: 'Medico no existe',
                        errors: { message: 'Medico no existe' }
                    });
                }
                var pathOld = './uploads/doctors/' + doctor.img;
                if (fs.existsSync(pathOld)) {
                    fs.unlink(pathOld);
                }
                doctor.img = fileName;
                doctor.save((err, doctorUpdated) => {
                    return res.status(200).json({
                        ok: true,
                        mensaje: 'Imagen de Medico actualizada',
                        doctor: doctorUpdated
                    });
                });

            });
            break;
        case 'hospitals':
            Hospital.findById(id, (err, hospital) => {
                if (!hospital) {
                    return res.status(400).json({
                        ok: true,
                        mensaje: 'Hospital no existe',
                        errors: { message: 'Hospital no existe' }
                    });
                }
                var pathOld = './uploads/hospitals/' + hospital.img;
                if (fs.existsSync(pathOld)) {
                    fs.unlink(pathOld);
                }
                hospital.img = fileName;
                hospital.save((err, hospitalUpdated) => {
                    return res.status(200).json({
                        ok: true,
                        mensaje: 'Imagen de Hospital actualizada',
                        hospital: hospitalUpdated
                    });
                });

            });
            break;
        default:
            return;
    }
}

module.exports = app;