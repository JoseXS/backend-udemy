var express = require('express');
var mongoose = require('mongoose');
var bodyParser = require('body-parser');

var app = express();

// body parser
// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

var appRoutes = require('./routes/app');
var userRoutes = require('./routes/user');
var hospitalRoutes = require('./routes/hospital');
var doctorRoutes = require('./routes/doctor');
var searchRoutes = require('./routes/search');
var imghRoutes = require('./routes/img');
var uploadRoutes = require('./routes/upload');
var loginRoutes = require('./routes/login');

app.use('/', appRoutes);
app.use('/user', userRoutes);
app.use('/hospital', hospitalRoutes);
app.use('/doctor', doctorRoutes);
app.use('/search', searchRoutes);
app.use('/img', imghRoutes);
app.use('/upload', uploadRoutes);
app.use('/login', loginRoutes);


// Conexion BD
mongoose.connection.openUri('mongodb://localhost:27017/hospitalDB', (err, res) => {
    if (err) throw err;
    console.log('BD: \x1b[32m%s\x1b[0m', ' Online');
});

app.listen(3333, () => {
    console.log('Express: \x1b[32m%s\x1b[0m', ' Online');
    console.log('puerto 3333');
});